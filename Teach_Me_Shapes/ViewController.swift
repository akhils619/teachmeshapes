//
//  ViewController.swift
//  Teach_Me_Shapes
//
//  Created by Akhil S Raj on 2019-11-04.
//  Copyright © 2019 Akhil S Raj. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    
    let USERNAME = "akhils619@gmail.com"
    let PASSWORD = "Flyhigh@619"
    
    let DEVICE_ID = "440035001047363333343437"
    var myPhoton : ParticleDevice?
    var imageName = ""
    
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var shapesImageView: UIImageView!
    
    @IBAction func nextImageButtonPressed(_ sender: Any) {
        startButton.setTitle("Next Image", for: .normal)
        displayRandomImages()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ParticleCloud.init()
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                print(error?.localizedDescription as Any)
            }
            else {
                print("Login success!")
                self.loadDeviceFromCloud()
            }
        }
    }
    
    func loadDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription as Any)
                return
            }
            else {
                print("Got photon from cloud: \(String(describing: device?.id))")
                self.myPhoton = device
                self.subscribeToParticleEvents()
            }
        }
    }
    
    func subscribeToParticleEvents() {
        print("Inside subscribe method")
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "shape",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    if (choice == "rectangle") {
                        if self.imageName == "rectangle"{
                            print("Inside rectangle code")
                            self.isAnswerCorrect()
                        }
                        else {
                            self.isAnswerWrong()
                        }
                    }
                    else if (choice == "triangle") {
                        if self.imageName == "triangle"{
                            print("Inside triangle code")
                            self.isAnswerCorrect()
                        }
                        else {
                            self.isAnswerWrong()
                        }
                    }
                    else if (choice == "circle") {
                        if self.imageName == "circle"{
                            print("Inside circle code")
                            self.isAnswerCorrect()
                        }
                        else {
                            self.isAnswerWrong()
                        }
                    }else {
                        self.rotateImage(angle: Float(choice) as! Float)
                    }
                }
        })
    }
    
    func isAnswerCorrect() {
        print("Pressed the change lights button")
        let parameters = ["right"]
        _ = myPhoton!.callFunction("displayLights", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
    }
    
    func isAnswerWrong() {
        print("Pressed the change lights button")
        let parameters = ["wrong"]
        _ = myPhoton!.callFunction("displayLights", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
    }
    
    func displayRandomImages(){
        self.imageName = generateRandomImages()
        self.shapesImageView.image = UIImage(named: imageName)
    }
    
    func generateRandomImages()->String{
        let shapes: [String] = ["rectangle", "triangle", "circle"]
        return shapes[Int.random(in: 0 ..< shapes.count)]
    }
    
    func rotateImage(angle: Float){
        UIView.animate(withDuration: 2.0, animations: {
            DispatchQueue.main.async {
                self.shapesImageView.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
                
            }
            
        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
}

